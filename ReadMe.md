# Overview
gtwelve micro is a collection of my favorite code put togeather in a convenient micro framework. It's prefect for microsites or simple apps.

## Installation
1. Clone the repository: `git clone git@bitbucket.org:gtwelve/gtwelve-micro.git`
2. Run `composer install` (assuming you have composer installed globally)
3. Open up `config.php` to configure timezone, debug mode and database settings. You can add as many values as your require and access them in your controllers by calling `$this->app->config('application')`
4. Open `routes.php` to add new routes
5. Controllers, Models and Views are located in `src/App/` folder.