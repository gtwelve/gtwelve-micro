<?php

// Constants
define('WEBROOT', dirname(__FILE__));
define('APPROOT', dirname(WEBROOT));
define('APP_PATH', APPROOT . '/');
define('SLIM_ENV', (getenv('SLIM_ENV') ? getenv('SLIM_ENV') : 'production'));

// Load the config file
$config = require APPROOT . '/config.php';

// Configure date
date_default_timezone_set($config['timezone']);

// Include core engine files as defined in composer.json
require APPROOT . '/vendor/autoload.php';

// If DB set in the config configure orm
if (isset($config['db_config']) && isset($config['db_config'][SLIM_ENV])) {
	ORM::configure($config['db_config'][SLIM_ENV]['dsn']);
	ORM::configure('username', (isset($config['db_config'][SLIM_ENV]['username']) ? $config['db_config'][SLIM_ENV]['username'] : NULL));
	ORM::configure('password', (isset($config['db_config'][SLIM_ENV]['password']) ? $config['db_config'][SLIM_ENV]['password'] : NULL));
}

// init app
$app = new \SlimController\Slim(array(
	'mode'							=> SLIM_ENV,
    'templates.path'				=> APP_PATH . 'src/App/View',
    'controller.class_prefix'		=> '\App\Controller',
    'controller.method_suffix'		=> '_action',
    'controller.template_suffix'	=> 'twig'
));

// Set custom config
$app->config('application', $config);

// Setup custom Twig view
$app->view(new \Slim\Views\Twig());
$app->view->parserOptions = array(
    'charset' => 'utf-8',
    'cache' => APP_PATH . 'cache/twig',
    'auto_reload' => true,
    'strict_variables' => false,
    'autoescape' => true
);
$app->view->parserExtensions = array(new \Slim\Views\TwigExtension());

// Set the routes
$routes = require APPROOT . '/routes.php';
$app->addRoutes($routes);

// set 404
$app->notFound(function () use ($app) {
	$app->response()->status(404);
	return $app->render('404.twig');
});

$app->run();