<?php

namespace App\Model;

/**
 * Base Model
**/
class Base extends \Model {
	
	public static function query()
	{
		return \Model::factory(get_called_class());
	}
	
	public static function create($data = NULL)
	{
		return \ORM::for_table(static::$_table)->create($data);
	}
	
	public static function loadById($id, $as_array = false)
	{
		$q = static::query()->where_equal('id', $id)->find_one();
		return $q == false ? false : ($as_array ? $q->as_array() : $q) ;
	}
	
	public static function loadAll($as_array = false, $active = false)
	{
		$data = array();
		$q = $active ? static::query()->where_equal('active', 1) : static::query() ;
		$r = $as_array ? $q->find_array() : $q->find_many() ;
		
		foreach ($r as $i) {
			$data[(is_object($i) ? $i->id : $i['id'])] = $i;
		}
		
		return $data;
	}
	
	public static function loadActive($as_array = false)
	{
		return static::loadAll($as_array, true);
	}
	
}

