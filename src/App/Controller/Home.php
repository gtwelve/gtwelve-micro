<?php

namespace App\Controller;

class Home extends Base
{
	
	public function index_action()
	{
		return $this->renderView('index');
	}
	
}