<?php

namespace App\Controller;

class Base extends \SlimController\SlimController
{
	
	public function renderView($template, $args = array())
	{
		if (!isset($args['is_ajax'])) { $args['is_ajax'] = $this->is_ajax(); }
		
		return $this->render($template, $args);
	}
	
	
	public function is_ajax() 
	{
		return (isset($_REQUEST['ajax']) || (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest"));
	}
	
	
	public function not_found()
	{
		$this->app->response()->status(404);
		return $this->renderView('404');
	}
	
}