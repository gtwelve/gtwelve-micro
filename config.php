<?php

// App configuration - Add as many values as needed
return array(
		'timezone' => 'Pacific/Auckland',
		'slim_config' => array(
		    'debug' => true
		),
		'db_config' => array(
			'development' => array(
				'dsn' => 'mysql:host=127.0.0.1;dbname=',
				'username' => 'root',
				'password' => ''
			),
			'production' => array(
				'dsn' => 'mysql:host=127.0.0.1;dbname=',
				'username' => '',
				'password' => ''
			)
		)
	);